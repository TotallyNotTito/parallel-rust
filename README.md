# Parallel Rust
### CS 510 - Programming in Rust
### Team Members: Jay Derderian, Tito Reinhart
### Project Repo: https://gitlab.com/TotallyNotTito/parallel-rust

# About

The objective of this project is to create a process manager in Rust. The process manager will be geared towards 
processing audio files with the help of auxiliary files. Rust is highly parallelized and can serve as an effective 
wrapper from paralleling programs to process audio data concurrently.

### Goals

The Minimum Viable Product we will aim for with this project will be a simple terminal-based application that will 
facilitate easy use of processing audio files with some basic audio effects. The application will be able to process 
multiple audio files simultaneously via multi-threading, using a process management system implemented in Rust.

The project will essentially consist of two core components: the Rust-based process manager, and the audio processing 
code to actually process the files.

### Development

Development will be managed by Tito and Jay. We will share a repo on Gitlab and will have a single “main” branch 
containing our final result. We will concurrently develop features by using our own separate development branches, 
and will make coordinated, incremental merges as the project progresses.

# Multi-threaded audio processing

This program is a multi-threaded script runner. It uses multiple threads to execute audio processing scripts concurrently. 
The scripts to be executed are passed as command-line arguments to this Rust program.

## Project Structure

This project is composed of several Rust modules:

```
parallel-rust/
|---src/
|   |---utils/
|   |   |---requirements.txt
|   |   |---sound_sample.py
|   |   |---sound_sample.rs
|   |---main.rs
|   |---chorus.rs
|   |---compress.rs
|   |---delay.rs
|   |---files.rs
|   |---reverb.rs
|---.gitignore
|---Cargo.lock
|---Cargo.toml
|---demo.mp4
|---README.md
```

## How it works

The program takes a list of Rust scripts from command line arguments. Each script will be run by a separate thread.

The number of threads created is based on the square root of a predefined constant (1024 in this case), with the aim 
of balancing the workload across threads.
The scripts are queued up and then processed by each thread, one by one, until the queue is empty.

The `run_audio_processor` function is used to execute an audio processing program with a specific argument. The
audio processing program is called `Command::new()` and passing the script, and file path of the audio file to be 
processed, to the function.

The output of the program is printed if the execution is successful; otherwise, an error message is displayed.

The program uses the standard Rust synchronization primitives (Arc, Mutex) to manage shared access to the queue of scripts.

## Running the program

First you will need to build the program with all the necessary dependencies. To do this, 
run the following commands

```shell
cd path/to/parallel-rust
cargo build
```

This will read the projects `Cargo.toml` file and download and install required libraries to 
run the program.

You can run the program with the following command, replacing script1.*, script2.*, etc. with your actual script paths:

```shell
cargo run main.rs compress.rs delay.rs ...
```

## Tests

All effects files have a corresponding test. Each test has two associated constants that a user must fill in
with a path to a test file and a path to an output file if they wish to run all tests.

To run all tests, run:

```
cd path/to/parallel-rust
cargo test
```

## Requirements

To run this program, you need the following installed:

    Rust: A system for building and packaging Rust projects. 

Please note that this program does not perform any checks for the existence or validity of the audio files, so make 
sure they exist and are accessible.