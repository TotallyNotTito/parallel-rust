use std::fs;

use hound::{WavReader, WavWriter, SampleFormat, WavSpec, Sample};

/// hardcode your own paths here if you want
const TEST_FILE_IN: &str = "C:\\Users\\Jay\\Google Drive\\School Work\\Rust\\project\\parallel-rust\\parallel-rust\\src\\utils\\Headstone.wav";
const TEST_FILE_OUT: &str = "C:\\Users\\Jay\\Google Drive\\School Work\\Rust\\project\\parallel-rust\\parallel-rust\\src\\test-chorus.wav";


struct ChorusConfig {
    delay: u32,
    depth: f32,
    speed: f32,
    feedback: f32,
}

impl ChorusConfig {
    fn new(delay: u32, depth: f32, speed: f32, feedback: f32) -> Self {
        ChorusConfig {
            delay,
            depth,
            speed,
            feedback,
        }
    }
}


fn apply_chorus_effect(input_file_path: &str, output_file_path: &str, config: ChorusConfig){
    // Open the input .wav file for reading
    let mut samples: Vec<f32> = WavReader::open(input_file_path)
        .unwrap()
        .into_samples::<f32>()
        .map(|s| s.unwrap_or(0.0))
        .collect();

    // Apply chorus effect to the samples
    let sample_rate = 44100 as f32;

    let chorus_samples: Vec<f32> = samples
        .into_iter()
        .enumerate()
        .map(|(i, sample)| {
            let delay = (config.depth * (2.0 * std::f32::consts::PI * config.speed * (i as f32) / sample_rate)
                .sin()) as u32;
            let _delayed_idx = if i >= delay as usize { i - delay as usize } else { i };
            let delayed_sample = sample + (sample * config.feedback);
            delayed_sample
        })
        .collect();

    // Create a new WAV file and write the modified samples
    let spec = WavSpec {
        channels: 2,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: SampleFormat::Float,
    };
    // write out samples
    let mut writer = WavWriter::create(output_file_path, spec).unwrap();
    for sample in chorus_samples {
        writer.write_sample(sample).unwrap();
    }
    writer.finalize().unwrap();
}


#[test]
fn test_apply_chorus_effect() {
    // Specify the chorus effect configuration
    let delay = 100;
    let depth = 0.5;
    let speed = 0.2;
    let feedback = 0.3;

    // Create the chorus configuration
    let config = ChorusConfig::new(delay, depth, speed, feedback);

    // Apply the chorus effect to the input file
    apply_chorus_effect(TEST_FILE_IN, TEST_FILE_OUT, config);

    // Read the output file
    let mut reader = WavReader::open(TEST_FILE_OUT).unwrap();
    let output_samples: Vec<f32> = reader
        .samples::<f32>()
        .into_iter()
        .map(|s| s.unwrap())
        .collect();

    // Assert that the outputted file isn't empty
    assert_ne!(output_samples.len(), 0);;
}