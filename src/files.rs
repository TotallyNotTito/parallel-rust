/// A file for all file utility methods
use std::fs;

/// hardcode your own file paths here
const TEST_FILE: &str = "v";
const TEST_DIR: &str = "";

/// get the file size in bytes
pub fn get_file_size(path: &str) -> usize {
    let metadata = fs::metadata(path)
        .expect("Failed to get metadata for file");
    metadata.len() as usize
}

/// count the number of files in a directory.
/// non-recursive. only counts top level contents
pub fn file_counter(dir_name: &str) -> usize {
    let mut count = 0;
    let paths = fs::read_dir(dir_name).unwrap();
    for path in paths {
        println!("Name: {}", path.unwrap()
            .path()
            .display());
        count += 1;
    }
    count
}


#[test]
fn test_get_file_size() {
    let test_file_size = get_file_size(TEST_FILE);
    assert_ne!(test_file_size, 0);
}

#[test]
fn test_file_counter() {
    let file_count = file_counter(TEST_DIR);
    assert_ne!(file_count, 0);
}
