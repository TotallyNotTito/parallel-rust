import numpy as np
from scipy.io.wavfile import write
import sounddevice as sd

def generate_sine_wave(sample_rate, frequency, amplitude, duration):
    samples = np.linspace(0, duration, duration * sample_rate, False)
    sine_wave = amplitude * np.sin(2 * np.pi * frequency * samples)
    return sine_wave.astype(np.int16)

def play_audio(samples, sample_rate):
    sd.play(samples, samplerate=sample_rate)
    sd.wait()

def main():
    sample_rate = 48000
    frequency = 440
    amplitude = 8192
    duration = 1

    sine_wave_samples = generate_sine_wave(sample_rate, 
                                           frequency, 
                                           amplitude, 
                                           duration)
    write('sine.wav', sample_rate, sine_wave_samples)

    clipped_wave_samples = np.clip(generate_sine_wave(sample_rate, 
                                                      frequency, 
                                                      amplitude * 2, 
                                                      duration), 
                                    -amplitude, 
                                    amplitude)
    write('clipped.wav', sample_rate, clipped_wave_samples)

    play_audio(clipped_wave_samples, sample_rate)
    play_audio(sine_wave_samples, sample_rate)

if __name__ == '__main__':
    main()
