extern crate cpal;
extern crate hound;
extern crate ndarray;

use cpal::traits::{DeviceTrait, EventLoopTrait, HostTrait};
use hound::WavWriter;
use ndarray::Array1;
use std::f64::consts::PI;
use std::i16;

fn generate_sine_wave(sample_rate: usize, frequency: f64, amplitude: f64, duration: f64) -> Array1<i16> {
    let samples: Array1<f64> = Array1::linspace(0.0, duration, duration as usize * sample_rate);
    let sine_wave = amplitude * (2.0 * PI * frequency * &samples).mapv(f64::sin);
    sine_wave.mapv(|x| x as i16)
}

fn main() {
    let sample_rate = 48000;
    let frequency = 440.0;
    let amplitude = 8192.0;
    let duration = 1.0;

    let sine_wave_samples = generate_sine_wave(sample_rate, frequency, amplitude, duration);
    let mut writer = WavWriter::create("sine.wav", hound::WavSpec {
        channels: 1,
        sample_rate: sample_rate as u32,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    })
    .unwrap();
    for sample in sine_wave_samples.iter() {
        writer.write_sample(*sample).unwrap();
    }

    let clipped_wave_samples = generate_sine_wave(sample_rate, frequency, amplitude * 2.0, duration)
        .mapv(|x| x.max(-amplitude as i16).min(amplitude as i16));
    let mut writer = WavWriter::create("clipped.wav", hound::WavSpec {
        channels: 1,
        sample_rate: sample_rate as u32,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    })
    .unwrap();
    for sample in clipped_wave_samples.iter() {
        writer.write_sample(*sample).unwrap();
    }

    let host = cpal::default_host();
    let device = host.default_output_device().unwrap();
    let format = device.default_output_format().unwrap();
    let event_loop = host.event_loop();
    let stream_id = event_loop.build_output_stream(&device, &format).unwrap();

    event_loop.play_stream(stream_id.clone()).unwrap();

    event_loop.run(move |_stream_id, stream_result| {
        let stream_data = match stream_result {
            Ok(data) => data,
            Err(err) => {
                eprintln!("An error occurred on stream {:?}: {}", stream_id, err);
                return;
            }
        };

        match stream_data {
            cpal::StreamData::Output { buffer: cpal::UnknownTypeOutputBuffer::U16(mut buffer) } => {
                for sample in sine_wave_samples.iter().chain(clipped_wave_samples.iter()) {
                    for elem in buffer.iter_mut() {
                        *elem = cpal::Sample::to_u16(sample);
                    }
                }
            }
            _ => (),
        }
    });
}
