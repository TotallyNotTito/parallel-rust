///! Parallel - Rust: a multi-threaded audio file processing tool.
///
///
mod files;
mod delay;
mod chorus;
mod compress;
mod reverb;

use std::fs;
use std::env;
use std::thread;
use std::sync::{Arc, Mutex};
use std::process::Command;
use crate::files::file_counter;


fn check_env() {
    // Collect the current environment variables
    let env_vars: Vec<(String, String)> = env::vars().collect();
    assert_ne!(env_vars.len(), 0)

    // make sure we have *at last* the commands
}


fn run_audio_processor(cmds: &[String], arg: &str) {
    // audio file
    let path = format!("{}", cmds[0]).to_string();
    println!("PATH: {:?} ARG: {}", path, arg);

    let output = Command::new("cargo")
        .arg("run")
        .arg("--bin")
        .arg(path)
        .arg(arg)
        .output()
        .expect("Failed to execute command");

    // Check the output of the subprocess
    if output.status.success() {
        let stdout = String::from_utf8_lossy(&output.stdout);
        println!("Subprocess executed successfully:\n{}", stdout);
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        println!("Subprocess failed:\n{}", stderr);
    }
}


fn threader(args: &[String]) {

    // let total_files: u64 = file_counter() as u64;
    let total_files = 1024 as u64;

    // build arc file queue from a list of file paths set as env variables
    let file_queue = Arc::new(Mutex::new(std::env::args()
        .skip(1)
        .collect::<Vec<String>>()
    ));

    // determine the potential number of threads using floor(sqrt(number))
    let num_threads = (total_files as f64)
            .sqrt()
            .floor() as u64;
    let args = Arc::new(args.to_vec());
    let mut threads = vec![];

    // Create multiple threads to call the script with different arguments
    for i in 0..num_threads {
        let arg = format!("Thread-{}", i);
        // clone the Arc to have another pointer to the queue.
        let file_queue_clone = Arc::clone(&file_queue);
        let handle = thread::spawn(move || {
            loop {
                let file_path = {
                    // lock the mutex (this block is necessary to limit the scope of the lock)
                    let mut queue = file_queue_clone
                        .lock()
                        .unwrap();
                    // remove a file path from the queue
                    queue.pop()
                };
                
                // if there was a file path, process it. else, break the loop.
                match file_path {
                    Some(path) => run_audio_processor(&[path], &arg),
                    None => break
                }
            }
        });
        
        threads.push(handle);
    }

    for handle in threads {
        handle.join().unwrap();
    }

    println!("All threads have completed.");
}


fn main() {
    let args: Vec<String> = env::args()
        .skip(1)
        .collect();

    threader(&args);
}

