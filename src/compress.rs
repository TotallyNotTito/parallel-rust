use std::fs;

use hound::{Error, WavReader, WavWriter};

// test constants
const TEST_FILE_IN: &str = "";
const TEST_FILE_OUT: &str = "";


fn apply_compression(input_path: &str, output_path: &str, threshold: f32, ratio: f32) -> Result<(), hound::Error> {
    // Read the input audio file
    let mut reader = WavReader::open(input_path)?;
    let spec = reader.spec();
    let sample_rate = spec.sample_rate;
    let num_channels = spec.channels as usize;
    let samples: Vec<Vec<f32>> = reader.samples::<i32>()
        .filter_map(Result::ok)
        .map(|sample| sample as f32 / i32::MAX as f32) // Convert to float
        .collect::<Vec<f32>>()
        .chunks(num_channels)
        .map(|chunk| chunk.to_vec())
        .collect();

    // Apply compression effect
    let compressed_samples: Vec<Vec<f32>> = samples
        .iter()
        .map(|channel| {
            // root mean square
            let rms: f32 = channel
                .iter()
                .map(|&sample| sample.powi(2))
                .sum::<f32>()
                .sqrt() / channel.len() as f32;
            let gain_reduction = if rms > threshold {
                1.0 - ((rms - threshold) * ratio).max(0.0)
            } else {
                1.0
            };
            channel
                .iter()
                .map(|&sample| sample * gain_reduction)
                .collect()
        })
        .collect();

    // Write the output audio file
    let mut writer = WavWriter::create(output_path, spec)?;
    for channel in compressed_samples {
        for sample in channel {
            let sample_value = (sample * i32::MAX as f32) as i32; // Convert back to i32
            writer.write_sample(sample_value)?;
        }
    }

    Ok(())
}

#[test]
fn test_apply_compression() {
    println!("[TEST] starting...");

    // Apply compression to the test file
    let threshold = 0.4;
    let ratio = 0.8;
    let res = apply_compression(TEST_FILE_IN, TEST_FILE_OUT, threshold, ratio);
    match res {
        Err(e) => println!("[TEST] failed to apply compression: {}", e),
        Result => println!("success: {:?}", Result),
    }

    // Read the compressed audio file
    let mut reader = hound::WavReader::open(TEST_FILE_OUT).unwrap();
    let compressed_samples: Vec<f32> = reader
        .samples::<f32>()
        .map(Result::unwrap)
        .collect();

    println!("[TEST] loaded compressed audio: {:?}", compressed_samples);

    // Assert that we actually opened the file and were able to write samples
    assert_ne!(compressed_samples.len(), 0);
}