/// Module for applying a reverb effect to a .wav file.
use std::fs;

use hound::{Error, WavReader, WavSpec, WavWriter, SampleFormat};

const REVERB_TIME: usize = 44100; // Reverb time in samples
const TEST_FILE_IN: &str = "";
const TEST_FILE_OUT: &str = "";

fn apply_reverb(input_path: &str, output_path: &str) -> Result<(), Error> {
    // Open the input .wav file for reading
    let mut samples: Vec<f32> = WavReader::open(input_path)
        .unwrap()
        .into_samples::<f32>()
        .map(|s| s.unwrap_or(0.0))
        .collect();

    // Create a new .wav file for writing the output
    let spec = WavSpec {
        channels: 2,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: SampleFormat::Float,
    };
    let mut writer = WavWriter::create(output_path, spec).unwrap();

    // Read and process audio samples
    for sample in &samples {
        let mut output_sample = *sample;

        // Apply reverb effect
        for i in 1..REVERB_TIME {
            // prevent indexing sadness
            if i+1 >= REVERB_TIME {
                break;
            }
            let delayed_sample = samples[i+1];
            output_sample += 0.5f32.powi(i as i32) * delayed_sample;
        }

        // Write the processed sample to the output .wav file
        writer.write_sample(output_sample).unwrap();
    }

    Ok(())
}


#[test]
fn test_apply_reverb() {
    // try to apply reverb effect to a test file
    let result = apply_reverb(TEST_FILE_IN, TEST_FILE_OUT);

    // Assert
    assert!(result.is_ok());
    assert!(fs::metadata(TEST_FILE_OUT).is_ok());

}
