use std::i16;
use std::fs;

use hound;


fn add_delay(file_path: &str, output_path: &str, decay: f32){
    // Open the input file
    let reader = hound::WavReader::open(file_path).unwrap();
    let spec = reader.spec();
    let samples: Vec<i16> = reader.into_samples::<i16>().collect::<Result<Vec<_>, _>>().unwrap();

    // Apply delay effect to the samples
    let delayed_samples: Vec<i16> = samples
        .iter()
        .enumerate()
        .map(|(i, &sample)| {
            let delayed_sample = if i < samples.len() as usize {
                sample
            } else {
                let delayed_idx = i - samples.len() as usize;
                let delayed_sample = sample + (samples[delayed_idx] as f32 * decay) as i16;
                // Ensure the sample value does not exceed the bounds
                if delayed_sample == i16::MAX {
                    i16::MAX
                } else if delayed_sample == i16::MIN {
                    i16::MIN
                } else {
                    delayed_sample
                }
            };
            delayed_sample
        })
        .collect();

    // Create a new WAV file and write the modified samples
    let mut writer = hound::WavWriter::create(output_path, spec).unwrap();
    for sample in delayed_samples {
        writer.write_sample(sample).unwrap();
    }
    writer.finalize().unwrap();
}

/// Tests
#[test]
fn test_apply_delay_effect() {
    // Set up test input file
    let input_file_path = "test_input.wav";
    let output_file_path = "test_output.wav";

    // Create a test .wav file
    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };

    // Generate a test audio signal
    let audio_samples: Vec<i16> = (0..44100)
        .map(|t| (t as f64 * 440.0 * 2.0 * std::f64::consts::PI / 44100.0).sin() * 32767.0)
        .map(|sample| sample as i16)
        .collect();

    // Create the input test .wav file
    let mut writer = hound::WavWriter::create(input_file_path, spec).unwrap();
    for sample in audio_samples.iter() {
        writer.write_sample(*sample).unwrap();
    }
    writer.finalize().unwrap();

    // Apply the delay effect
    add_delay(input_file_path, output_file_path, 3.0);

    // Read the output file
    let reader = hound::WavReader::open(output_file_path).unwrap();
    let output_samples: Vec<i16> = reader.into_samples::<i16>().collect::<Result<Vec<_>, _>>().unwrap();

    // Assert the number of samples is the same
    assert_eq!(output_samples.len(), audio_samples.len());

    // Clean up the test files
    fs::remove_file(input_file_path).unwrap();
    fs::remove_file(output_file_path).unwrap();
}
